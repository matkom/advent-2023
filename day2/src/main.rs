use std::{cmp::max, convert::identity, fs, io};

fn main() -> io::Result<()> {
    let input = fs::read_to_string("./input.txt")?;
    part_one(&input);
    part_two(&input);
    Ok(())
}

fn part_one(input: &str) {
    let result: u32 = input
        .lines()
        .zip(1..)
        .filter_map(|(line, id)| {
            line.split_once(':')
                .unwrap()
                .1
                .split(|c| c == ';' || c == ',')
                .map(|cube| {
                    let (amount, color) = cube.trim().split_once(' ').unwrap();
                    let treshold = match color {
                        "red" => 12,
                        "green" => 13,
                        "blue" => 14,
                        _ => unreachable!(),
                    };
                    amount.parse::<u32>().unwrap() <= treshold
                })
                .all(identity)
                .then(|| id)
        })
        .sum();

    assert_eq!(2679, result);
    println!("{}", result);
}

fn part_two(input: &str) {
    let result: u32 = input
        .lines()
        .map(|line| {
            line.split_once(':')
                .unwrap()
                .1
                .split(|c| c == ';' || c == ',')
                .fold([0, 0, 0], |mut acc, cube| {
                    let (amount, color) = cube.trim().split_once(' ').unwrap();
                    let idx = match color {
                        "red" => 0,
                        "green" => 1,
                        "blue" => 2,
                        _ => unreachable!(),
                    };
                    acc[idx] = max(acc[idx], amount.parse().unwrap());
                    acc
                })
                .iter()
                .product::<u32>()
        })
        .sum();

    assert_eq!(77607, result);
    println!("{}", result);
}
