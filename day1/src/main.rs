use std::{fs, io, mem};

fn main() -> io::Result<()> {
    let input = fs::read_to_string("./input.txt")?;
    part_one(&input);
    part_two(&input);
    Ok(())
}

fn part_one(input: &str) {
    let result: u32 = input
        .lines()
        .map(|line| {
            line.chars()
                .filter(|c| c.is_numeric())
                .collect::<Vec<char>>()
        })
        .collect::<Vec<_>>()
        .iter()
        .map(|digits| match (digits.first(), digits.last()) {
            (Some(a), Some(b)) => {
                let number = format!("{}{}", a, b);
                number.parse::<u32>().unwrap_or_default()
            }
            _ => 0,
        })
        .sum();

    assert_eq!(55447, result);
    println!("Part one: {}", result);
}

fn part_two(input: &str) {
    let mut num_parser = NumberParser::default();
    let result: u32 = input
        .lines()
        .map(|line| num_parser.parse(line))
        .collect::<Vec<_>>()
        .iter()
        .map(|digits| match (digits.first(), digits.last()) {
            (Some(a), Some(b)) => {
                let number = format!("{}{}", a, b);
                number.parse::<u32>().unwrap_or_default()
            }
            _ => 0,
        })
        .sum();

    assert_eq!(54706, result);
    println!("Part two: {}", result);
}

#[derive(Default)]
struct NumberParser<'parser> {
    cursor: usize,
    input: &'parser str,
    result: Vec<char>,
}

impl<'parser> NumberParser<'parser> {
    fn parse(&mut self, input: &'parser str) -> Vec<char> {
        self.cursor = 0;
        self.input = input;

        loop {
            match self.get_next_char() {
                Some("o") => self.check_number('1', "ne"),
                Some("e") => self.check_number('8', "ight"),
                Some("n") => self.check_number('9', "ine"),
                Some("t") => {
                    self.advance();
                    match self.get_next_char() {
                        Some("w") => self.check_number('2', "o"),
                        Some("h") => self.check_number('3', "ree"),
                        _ => continue,
                    }
                }
                Some("f") => {
                    self.advance();
                    match self.get_next_char() {
                        Some("o") => self.check_number('4', "ur"),
                        Some("i") => self.check_number('5', "ve"),
                        _ => continue,
                    }
                }
                Some("s") => {
                    self.advance();
                    match self.get_next_char() {
                        Some("i") => self.check_number('6', "x"),
                        Some("e") => self.check_number('7', "ven"),
                        _ => continue,
                    }
                }
                Some(c) => {
                    let c = c.chars().next().unwrap();
                    if c.is_numeric() {
                        self.result.push(c);
                    }
                    self.advance();
                }
                None => break,
            }
        }

        mem::take(&mut self.result)
    }

    fn check_number(&mut self, num: char, pattern: &str) {
        self.advance();

        let end = self.cursor + pattern.len();
        if end > self.input.len() {
            return;
        }

        if &self.input[self.cursor..end] == pattern {
            self.cursor += pattern.len() - 1;
            self.result.push(num);
        }
    }

    fn advance(&mut self) {
        self.cursor += 1;
    }

    fn get_next_char(&self) -> Option<&str> {
        if self.cursor + 1 > self.input.len() {
            return None;
        }
        Some(&self.input[self.cursor..self.cursor + 1])
    }
}
